package sample;

import javafx.scene.input.KeyCode;
import sample.model.Model;

public class InputHandler {

    // Eigenschaften
    private Model model;

    // Konstruktoren
    public InputHandler(Model model) {
        this.model = model;
    }

    // Methoden
    public void onKeyPressed(KeyCode key) {
        if (key == KeyCode.LEFT) {
            if (model.getSpieler().getPosX() > 50) {   //Verhindert dass der Spieler raus gehen kann
                model.getSpieler().move(10);
            }
        } else if (key == KeyCode.RIGHT) {
            if (model.getSpieler().getPosX() <900) {// siehe oben
                model.getSpieler().move(-10);
//                System.out.println("rechts");
            }
        }
    }


}
