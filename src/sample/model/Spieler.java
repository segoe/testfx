package sample.model;

public class Spieler {
    //Eigenschaften
    private int posX;
    private int posY;
    private int hoehe;
    private int breite;
    //Konstruktoren

    /**
     * Erstelle den Spieler mit Position und Größe
     * @param posX
     * @param posY
     * @param hoehe
     * @param breite
     */
    public Spieler(int posX, int posY, int hoehe, int breite) {
        this.posX = posX;
        this.posY = posY;
        this.hoehe = hoehe;
        this.breite = breite;
    }

    //Methoden

    /**
     * Verändert die Position in X  Richtung
     * @param posX
     */
    public void move (int posX){
        this.posX -= posX;
    }

    //gettersetter (Nur für die Position)

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }
}
