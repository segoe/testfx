package sample.model;

public class Ball {
    //Eigenschaften
    private int posX;
    private int posY;
    private int hoehe;
    private int breite;
    private double speed;
    //Konstruktoren

    /**
     * Erstelle den Ball mit Position und Größe und Geschwindigkeit (double)
     * @param posX
     * @param posY
     * @param hoehe
     * @param breite
     */
    public Ball(int posX, int posY, int hoehe, int breite, double speed) {
        this.posX = posX;
        this.posY = posY;
        this.hoehe = hoehe;
        this.breite = breite;
        this.speed = speed;

    }

    //Methoden

    /**
     * Beschreibt die Bewegung des Balles
     */
    public void move(long elapsedTime, double yrichtung, double xrichtung){

        this.posY = Math.round((int)(this.posY + elapsedTime*yrichtung*speed)); //Hier aufpassen. hatte zuerst elaspedTime*(int)speed. aber das sit blöd wenn speed 0,irgendwas ist
        this.posX = Math.round((int)(this.posX + elapsedTime*xrichtung));
    }

    //gettersetter (Nur für die Position)

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }
}
