package sample.model;


public class Model {
    //Eigenschaften
    private boolean getroffen = false;

    public boolean isGetroffen() {
        return getroffen;
    }

    public static int breite = 1000;
    public static int hoehe = 600;
    private Spieler spieler;
    private Ball ball;
    private double speed;
    public boolean faellt;  // nur zum testen auf public
    private int anzahl = 1;
    private boolean rechts;

    public void setV(double v) {
        this.v = v;
    }

    private double v = 1;

    // Konstruktoren
    public Model() {
        this.ball = new Ball(50, 50, 100, 100, v);
        this.spieler = new Spieler(500, 550, 500, 250);
        faellt = true;
        rechts = true;
//        this.spieler = new Spieler(500, 100, 500, 250);
    }

    // Methoden
    double xrichtung = 0.1;

    /**
     * @param elapsedTime ständiges erneuern der Spielwelt durch den Timer
     */
    public void update(long elapsedTime) {  // DIESE ÄNDERUNGEN EVENTUELL DURCH GRAPHICS IMPLEMENTIEREN; BIN MIR ABER NICHT SICHER; Alex fragen!
        if (rechts) {
            if (ball.getPosX() > 950) {
                xrichtung = -xrichtung;
                rechts = !rechts;
            }
        }

        if (!rechts) {
            if (ball.getPosX() < 50) {
                xrichtung = -xrichtung;
                rechts = !rechts;
            }
        }

        if (faellt) {
            ball.move(elapsedTime, 0.2 * v, xrichtung * v);               //HIER DIE GESCHWINDIGKEIT MIT V ANGEPASST
            if (ball.getPosY() > 550) {
                faellt = !faellt;
                System.out.println(anzahl++ + " mal die Welt gerettet!");
                v *= 1.3;
            }

        }
        if (!faellt) {
            ball.move(elapsedTime, -0.1 * v, xrichtung * v);
            if (ball.getPosY() < 0) {
                faellt = !faellt;
//                System.out.println("er steigt");
            }
        }

//       beenden bei Collision
        if (ball.getPosX() < spieler.getPosX() + 200 && ball.getPosX() > spieler.getPosX()-50) {                     //Stopp Bedingung funktioniert hier prima ;)
            if (ball.getPosY() > 510) {
                getroffen = true;
            }                      //Timer greift hier auf graphics zu mit boolean

        }
    }

    //Setter Getter
    public Spieler getSpieler() {

        return this.spieler;
    }

    public Ball getBall() {
        return this.ball;
    }


}


