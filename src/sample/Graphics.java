package sample;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import sample.model.Ball;
import sample.model.Model;
import sample.model.Spieler;


public class Graphics {
//    public void setGetroffen(boolean getroffen) {
//        this.getroffen = getroffen;
//    }

    //Eigenschaften
    private int i = 1;
    private Model model;
    private GraphicsContext gc;
    private Ball ball;
    private Spieler spieler;

    //Konstrutkoren
    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;

    }

//    public boolean isGetroffen() {
//        return getroffen;
//    }

    // Methoden
    public void draw() {

        // Clear Screen
        gc.clearRect(0, 0, Model.breite, Model.hoehe);
        gc.setFill(Color.LIGHTBLUE);
        gc.fillRect(0, 0, Model.breite, Model.hoehe);
        // Draw Ball

        gc.setFill(Color.BLACK);
        gc.fillOval(model.getBall().getPosX(), model.getBall().getPosY(), 10, 10);

        //Draw Spieler
        gc.setFill(Color.BLUE);
        gc.fillRect(model.getSpieler().getPosX(), model.getSpieler().getPosY(), 100, 100);

        //Draw Ball
        Image earth = new Image("sample/earth3.png");
        gc.drawImage(earth, model.getBall().getPosX() - 20, model.getBall().getPosY() - 20);

        //Draw Player
        Image earth2 = new Image("sample/earth2.png");
        gc.drawImage(earth2, model.getSpieler().getPosX() - 20, model.getSpieler().getPosY() - 20);


        if (model.isGetroffen()) {
            Image earth3 = new Image("sample/earthexploded.png");
            gc.drawImage(earth3, model.getSpieler().getPosX() - 20, model.getSpieler().getPosY() - 20);
            model.setV(0);
            if (i == 1) {
                System.out.println("Du wurdest getroffen");
                i++;
                System.out.println("Leider sind jetzt alle tot!");
                System.out.println("Position X und Y Asteroid: " + model.getBall().getPosX() + " " + model.getBall().getPosY());
                System.out.println("Position X und Y Welt: " + model.getSpieler().getPosX() + " " + model.getSpieler().getPosY());
            }
        }

    }
}


