package sample;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;
import sample.model.Model;

public class MainSpiel extends Application {

    //Eigenschaften
    private Timer timer;


//    public static void main(String[] args) {
//        launch(args);
//    }

    public void start(Stage stage) throws Exception {
        stage.setTitle("Kopf Hoch!");

        // Canvas
        Canvas canvas = new Canvas(Model.breite, Model.hoehe);

        // Group
        Group group = new Group();
        group.getChildren().add(canvas);

        //Scene
        Scene scene = new Scene(group);

        // Stage
        stage.setScene(scene);
        stage.show();


        // Draw
        GraphicsContext gc = canvas.getGraphicsContext2D();
        Model model = new Model();
        Graphics graphics = new Graphics(model, gc); //Modelle sollten da sein ...
        timer = new Timer(model, graphics);
        timer.start();

        // InputHandler
        InputHandler inputHandler = new InputHandler(model);
        scene.setOnKeyPressed(
                event -> inputHandler.onKeyPressed(event.getCode())
        );

    }
}